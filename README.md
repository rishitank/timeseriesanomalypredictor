
Time-Series Anomaly prediction software
=======================================
Welcome to my Time-Series Anomaly Predictor software developed for my Undergraduate Final Year Project.

Author: Rishi Tank
Supervisor: Dr Artur Garcez
Year: 2011/2012

System Requirements:
Display resolution of 1474 x 768 or higher.
128MB RAM or better.
64MB graphics card or better.
1.8GHz CPU or better.
Windows 7 OS or better.
At least 100MB hard disk space.

SOFTWARE SOURCE CODE
====================
To see the software source code please go to the SRC directory and open the files using notepad where most of the code is in Main.cpp. If you have Microsoft Visual Studio 2010, feel free to open the .sln file to open the software project and build the project, although the project is prebuilt with the executables in the Bin directory for the marker's convenience. If you would like to build the project executable anyway, in Visual Studio 2010 open the .sln file and click Build > Clean Solution and then press the run button indicated by a green triangle symbol to compile, link and build the executable.

RUNNING THE SOFTWARE
====================
INSTRUCTIONS CBF Dataset:
=========================
1. Load the software by double clicking the exe file found in the Bin directory.
2. Click the browse button on the top right corner and locate the cbf_timeseries.ascii file found in the data directory.
3. Enter the following parameters without quotes: cluster = "3", window width = "100", sliding window width = "100", DCT sample size = "100" or "15" and time period = "100,200"
4. Click the Apply Changes button directly below.
5. Rules will be produced from an alphabet which can be exported using the Save Alphabet button.
6. Select what to display by clicking the checkboxes on the GUI to see the various outputs.
7. You may zoom in and slide up/down or right/left to see more data.

INSTRUCTIONS ECG Dataset:
=========================
1. Load the software by double clicking the exe file found in the Bin directory.
2. Click the browse button on the top right corner and locate the 14046_modified.ascii file found in the data directory.
3. Enter the following parameters without quotes: cluster = "2", window width = "32", sliding window width = "16", DCT sample size = "32" (can be lower) and time period = "1,2,3"
4. Click the Apply Changes button directly below.
5. Click the Zoom spinner down once so that the zoom level is roughly -0.15 and drag the vertical scrollbar upwards until the ECG time-series is in view.
6. Rules will be produced from an alphabet which can be exported using the Save Alphabet button and depends on which signal is displayed.
7. Select what to display by clicking the checkboxes on the GUI to see the various outputs as well as clicking the drop down box below to change which signal is currently being viewed as the ECG dataset contains two signals.
8. You mary slide right/left to see more data.

TROUBLESHOOTING
===============
Why is the software is taking a long time to process the data?
Please be aware that by inputting a sliding window width that is significantly smaller than the window width, the software will freeze and take a long time to process the data so either exit the software or patiently wait and let the software run. This may also produce meaningless subsequence cluster centers.

I cannot see the time-series graph!
Please use the zoom level spinner and the scrollbars to adjust the view and find a comfortable scale for your time-series, it is there but might not be in the current view.

I get an error when I try to load my dataset!
This might be due to a dataset in a format unsupported by this software. Currently the software can load datasets that are in tab delimited text files with each line holding a value from the dataset.

What are the safest parameters to use?
Typically, the number of clusters will correspond to the number of patterns you know that exist in your time-series. The window width is the typical length of this pattern and the sliding window width can be the same as the window width for non-overlapping or slightly less than the window width, but be aware not to freeze the program!
