#ifndef _GLPANE_H
#define _GLPANE_H
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/button.h>
#include <wx/statline.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/scrolbar.h>
#include <wx/event.h>
#include <wx/listctrl.h>
#include <wx/combobox.h>
#include <wx/checklst.h>
#include <wx/spinctrl.h>
#include <wx/file.h>
#include <wx/checkbox.h>
#include <wx/utils.h>

#ifdef _WIN64
	#include "../include/gl/glut.h"
#elif __APPLE__
	#include <GLUT/glut.h>
#elif __linux
	#include <GL/glut.h>
	#include <GL/glu.h>
	#include <GL/gl.h>
#endif

class GLPane : public wxGLCanvas
{
	private:
		wxGLContext*	m_context;
	 
	public:
		GLPane(wxFrame* parent, int* args);
		virtual ~GLPane();
	 
		void resized(wxSizeEvent& evt);
	 
		int getWidth();
		int getHeight();
	 
		void render(wxPaintEvent& evt);
		void OnIdle(wxIdleEvent& evt);
		void keyPressed(wxKeyEvent& event);
		void mouseWheelMoved(wxMouseEvent& event);

		DECLARE_EVENT_TABLE()
	
};

#endif
