#ifndef _MAIN_H
#define _MAIN_H

#include <map>
#include <stdio.h>
#include <stdlib.h>
#include "Helper.h"
#include "GLPane.h"
#include <sndfile.h>

using namespace std;

int SCREEN_WIDTH = 1600, SCREEN_HEIGHT = 800, numClusters, windowWidth, 
	slidingWinWidth, DCTSampleSize, numArrays, window, *orig, xaxis, yaxis, *DCTGraph, 
	*idctGraph, *clusterCentroids, **motifs, *discreteSeries, graphColourLabel, 
	blocksCreated = 0, view = 0, listBuilt = 0, movement = 0, origCheck = 1, dctCheck = 0, 
	inverseCheck = 0, motifsCheck = 0, discreteCheck = 0, GL_WIDTH=SCREEN_WIDTH-290, GL_HEIGHT=SCREEN_HEIGHT-80;
//const int SystemResWidth = glutGet(GLUT_SCREEN_WIDTH);
//const int SystemResHeight = glutGet(GLUT_SCREEN_HEIGHT);
vector<int> inferenceTimePeriod;
vector< vector<string> > clusterAlphabet;
float offsetX = 0.0f, offsetY = 0.0f, scaleY = 0.0f, maxOrig = 0.0f, 
	minOrig = 0.0f, maxSub = 0.0f, minSub = 0.0f, sbV_Range = 5000.0f, sbV_Start = sbV_Range/2;
vector< vector<double> > timeseries;
size_t dataSize, maxArrays;
string filename = "";
wxPanel *status_panel;
wxButton *browseBtn, *applyBtn, *saveAlphaBtn, *resetBtn;
wxTextCtrl *clustersText, *winWidthText, *slidingWinText, *DCTSampleText, *timePeriodText;
wxScrollBar *sbH, *sbV;
wxStaticText *browseText, *statusText, *messagesText;
wxListCtrl *rulesList, *frequencyList;
wxCheckListBox *displayCLB;
wxSpinCtrlDouble *zoomSpinner;
wxComboBox *columnSelect, *dataTypeSelect;
string dataType = "";
map<string,double> symbolFreq, symbolRelFreq;
map<Keys*,double> ruleFreq, ruleConf;
bool freqDisplayed = false, header = false, fileChanged = true, normalise = false, isNormalised = false;
vector<string> signalLabels;
wxBoxSizer *vbox, *glSizer, *parentSizer;
wxCheckBox *normaliseCB;

void closeWindow()
{
	if (!timeseries.empty()) {
		for (unsigned int a = 0; a < maxArrays; a++) {
			timeseries.erase(timeseries.begin());
		}
		columnSelect->Clear();
	}

	dataTypeSelect->Clear();

	if (rulesList->GetItemCount() > 0) {
		rulesList->ClearAll();
	}
	
	if (frequencyList->GetItemCount() > 0) {
		frequencyList->ClearAll();
	}

	while (!inferenceTimePeriod.empty()) {
		inferenceTimePeriod.erase(inferenceTimePeriod.begin());
	}

	while (!clusterAlphabet.empty()) {
		clusterAlphabet.erase(clusterAlphabet.begin());
	}

	while (!signalLabels.empty()) {
		signalLabels.erase(signalLabels.begin());
	}
	
	//delete clustersText;
	//delete winWidthText;
	//delete slidingWinText;
	//delete DCTSampleText;
	//delete timePeriodText;
	//delete browseText;
	//delete statusText;
	//delete messagesText;
	//delete rulesList;
	//delete zoomSpinner;

	glDeleteLists(1, 20);

	exit(0);
}

wxFrame* parentFrame;

class MyApp : public wxApp
{
  public:
    virtual bool OnInit();
    void control_cb(wxCommandEvent& event);
    wxFrame *frame;
    GLPane *glPane;
    wxPanel *sidePanel;

};
DECLARE_APP(MyApp)

#endif
