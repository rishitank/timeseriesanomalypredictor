#include "StdAfx.h"
#include <math.h>
#include "Main.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	parentSizer = new wxBoxSizer(wxHORIZONTAL);
	glSizer = new wxBoxSizer(wxVERTICAL);
	frame = new wxFrame((wxFrame *)NULL, -1, wxT("Time Series Anomaly Predictor By Rishi Tank, 2012"), wxDefaultPosition, wxSize(SCREEN_WIDTH, SCREEN_HEIGHT));
	
	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 32, 0};
	while (!GLPane::IsDisplaySupported(args) && args[3] != 0) {
		args[3] -= 16;
	}

	glPane = new GLPane(frame, args);
	glSizer->Add(glPane, 0, wxALIGN_LEFT|wxALIGN_TOP);
	
	sidePanel = new wxPanel(frame, wxID_ANY);
	
	sbV = new wxScrollBar(frame, SCROLLBARV_ID, wxPoint(GL_WIDTH, 0), wxSize(20, GL_HEIGHT), wxSB_VERTICAL);
	Connect(sbV->GetId(), wxEVT_SCROLL_THUMBTRACK, wxCommandEventHandler(MyApp::control_cb));

	vbox = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer* clustersBox = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* winWidthBox = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* slidingWinBox = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* DCTSampleBox = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* timePeriodBox = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* browseBox = new wxBoxSizer(wxHORIZONTAL);

	string initMessage = "Please Select a dataset using the browse button. Then set parameters and click Apply.";
	messagesText = new wxStaticText(sidePanel, wxID_ANY, initMessage, wxDefaultPosition, wxDefaultSize);
	messagesText->Wrap(250);
	vbox->Add(messagesText, 0, wxALIGN_CENTER);
	vbox->Add(-1,5);

	browseBtn = new wxButton(sidePanel, BROWSE_BUTTON_ID, wxT("Browse"));
	Connect(browseBtn->GetId(), wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MyApp::control_cb));
	browseBox->Add(browseBtn, 0, wxLEFT|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	resetBtn = new wxButton(sidePanel, RESET_BUTTON_ID, wxT("Reset"));
	Connect(resetBtn->GetId(), wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MyApp::control_cb));
	resetBtn->Disable();
	browseBox->Add(resetBtn, 0, wxRIGHT|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL);
	
	vbox->Add(browseBox, 0, wxALIGN_CENTER, 10);
	vbox->Add(-1,10);

	wxBoxSizer *dtBox = new wxBoxSizer(wxHORIZONTAL);
	normaliseCB = new wxCheckBox(sidePanel, NORMALISE_CB_ID, wxT("Normalise"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	Connect(normaliseCB->GetId(), wxEVT_CHECKBOX, wxCommandEventHandler(MyApp::control_cb));
	dataTypeSelect = new wxComboBox(sidePanel, DATATYPESELECT_COMBOBOX_ID, "", wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_READONLY);
	dataTypeSelect->Append("Data Type:");
	dataTypeSelect->Append("Audio");
	dataTypeSelect->Append("ECG");
	dataTypeSelect->Append("EEG");
	dataTypeSelect->Append("Other");
	dataTypeSelect->SetSelection(0);
	Connect(dataTypeSelect->GetId(), wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler(MyApp::control_cb));
	dtBox->Add(normaliseCB, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	dtBox->Add(dataTypeSelect, 0, wxALIGN_LEFT);
	vbox->Add(dtBox, 0, wxALIGN_RIGHT);

	clustersText = new wxTextCtrl(sidePanel, CLUSTER_TEXTBOX_ID, "", wxDefaultPosition, wxDefaultSize);
	winWidthText = new wxTextCtrl(sidePanel, WINWIDTH_TEXTBOX_ID, "", wxDefaultPosition, wxDefaultSize);
	slidingWinText = new wxTextCtrl(sidePanel, SLIDING_TEXTBOX_ID, "", wxDefaultPosition, wxDefaultSize);
	DCTSampleText = new wxTextCtrl(sidePanel, DCTSAMPLE_TEXTBOX_ID, "", wxDefaultPosition, wxDefaultSize);
	timePeriodText = new wxTextCtrl(sidePanel, TIMEPERIOD_TEXTBOX_ID, ",", wxDefaultPosition, wxDefaultSize);

	Connect(clustersText->GetId(), wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(MyApp::control_cb));
	Connect(winWidthText->GetId(), wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(MyApp::control_cb));
	Connect(slidingWinText->GetId(), wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(MyApp::control_cb));
	Connect(DCTSampleText->GetId(), wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(MyApp::control_cb));
	Connect(timePeriodText->GetId(), wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(MyApp::control_cb));

	wxStaticText* clustersLabel = new wxStaticText(sidePanel, wxID_ANY, "Clusters:", wxDefaultPosition, wxDefaultSize);
	wxStaticText* winWidthLabel = new wxStaticText(sidePanel, wxID_ANY, "Window Width:", wxDefaultPosition, wxDefaultSize);
	wxStaticText* slidingWinLabel = new wxStaticText(sidePanel, wxID_ANY, "Sliding Window Width:", wxDefaultPosition, wxDefaultSize);
	wxStaticText* DCTSampleLabel = new wxStaticText(sidePanel, wxID_ANY, "DCT Sample Size:", wxDefaultPosition, wxDefaultSize);
	wxStaticText* timePeriodLabel = new wxStaticText(sidePanel, wxID_ANY, "Rule Time Period:", wxDefaultPosition, wxDefaultSize);

	applyBtn = new wxButton(sidePanel, APPLY_BUTTON_ID, wxT("Apply Changes"));
	Connect(applyBtn->GetId(), wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MyApp::control_cb));
	applyBtn->Disable();

	clustersBox->Add(clustersLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	clustersBox->Add(clustersText, 1, wxALIGN_LEFT);
	winWidthBox->Add(winWidthLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	winWidthBox->Add(winWidthText, 1, wxALIGN_LEFT);
	slidingWinBox->Add(slidingWinLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	slidingWinBox->Add(slidingWinText, 1, wxALIGN_LEFT);
	DCTSampleBox->Add(DCTSampleLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	DCTSampleBox->Add(DCTSampleText, 1, wxALIGN_LEFT);
	timePeriodBox->Add(timePeriodLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	timePeriodBox->Add(timePeriodText, 1, wxALIGN_LEFT);

	vbox->Add(clustersBox, 0, wxALIGN_RIGHT);
	vbox->Add(winWidthBox, 0, wxALIGN_RIGHT);
	vbox->Add(slidingWinBox, 0, wxALIGN_RIGHT);
	vbox->Add(DCTSampleBox, 0, wxALIGN_RIGHT);
	vbox->Add(timePeriodBox, 0, wxALIGN_RIGHT);
	vbox->Add(-1,10);
	vbox->Add(applyBtn, 0, wxALIGN_CENTER);
	vbox->Add(-1,5);

	rulesList = new wxListCtrl(sidePanel, RULES_LIST_ID, wxDefaultPosition, wxSize(100, 160), wxLC_REPORT);
	rulesList->InsertColumn(1, wxT("Signal"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE_USEHEADER);
	rulesList->InsertColumn(2, wxT("Rules"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE);
	rulesList->InsertColumn(3, wxT("Confidence"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE_USEHEADER);
	frequencyList = new wxListCtrl(sidePanel, FREQUENCY_LIST_ID, wxDefaultPosition, wxSize(80, 100), wxLC_REPORT);
	frequencyList->InsertColumn(1, wxT("Signal"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE_USEHEADER);
	frequencyList->InsertColumn(2, wxT("Pattern"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE_USEHEADER);
	frequencyList->InsertColumn(3, wxT("Frequency"), wxLIST_FORMAT_LEFT, wxLIST_AUTOSIZE_USEHEADER);
	saveAlphaBtn = new wxButton(sidePanel, SAVE_BUTTON_ID, wxT("Save Alphabet"));
	Connect(saveAlphaBtn->GetId(), wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MyApp::control_cb));
	saveAlphaBtn->Disable();

	vbox->Add(rulesList, 0, wxEXPAND|wxALIGN_CENTER);
	vbox->Add(-1,5);
	vbox->Add(frequencyList, 0, wxEXPAND|wxALIGN_CENTER);
	vbox->Add(-1,5);
	vbox->Add(saveAlphaBtn, 0, wxALIGN_CENTER);

	wxArrayString choices;
	choices.Add("Original Time Series");choices.Add("DCT Transformed");choices.Add("Inverse DCT");choices.Add("Motifs");choices.Add("Discrete Time Series");
	displayCLB = new wxCheckListBox(sidePanel, CHECKBOX_LIST_ID, wxDefaultPosition, wxSize(200,125), choices);
	Connect(displayCLB->GetId(), wxEVT_CHECKLISTBOX, wxCommandEventHandler(MyApp::control_cb));
	displayCLB->Check(0);
	vbox->Add(-1,5);
	vbox->Add(displayCLB, 0, wxALIGN_CENTER);

	vbox->Add(-1,5);

	wxBoxSizer *dsBox = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText* dsLabel = new wxStaticText(sidePanel, wxID_ANY, "Signal to Display:", wxDefaultPosition, wxDefaultSize);
	columnSelect = new wxComboBox(sidePanel, COLUMNSELECT_COMBOBOX_ID, "", wxDefaultPosition, wxSize(100,-1), 0, NULL, wxCB_DROPDOWN|wxCB_READONLY);
	Connect(columnSelect->GetId(), wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler(MyApp::control_cb));
	columnSelect->Disable();
	dsBox->Add(dsLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	dsBox->Add(columnSelect, 0, wxALIGN_LEFT);
	vbox->Add(dsBox, 0, wxALIGN_RIGHT);
	vbox->Add(-1,5);

	wxBoxSizer *zoomBox = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText* zoomLabel = new wxStaticText(sidePanel, wxID_ANY, "Zoom Y:", wxDefaultPosition, wxDefaultSize);
	zoomSpinner = new wxSpinCtrlDouble(sidePanel, ZOOM_SPINNER_ID, "0", wxDefaultPosition, wxSize(100,-1), wxSP_ARROW_KEYS, -500, 500, 0, 0.15);
	Connect(zoomSpinner->GetId(), wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxCommandEventHandler(MyApp::control_cb));
	zoomSpinner->Disable();
	zoomBox->Add(zoomLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL);
	zoomBox->Add(zoomSpinner, 0, wxALIGN_LEFT);
	vbox->Add(zoomBox, 0, wxALIGN_RIGHT);
	vbox->Add(-1,10);

	wxButton *quitBtn = new wxButton(sidePanel, QUIT_BUTTON_ID, wxT("Quit"));
	Connect(quitBtn->GetId(), wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MyApp::control_cb));
	vbox->Add(quitBtn, 0, wxALIGN_CENTER);

	sbH = new wxScrollBar(frame, SCROLLBARH_ID, wxPoint(0, 600), wxSize(GL_WIDTH, 20), wxSB_HORIZONTAL);
	Connect(sbH->GetId(), wxEVT_SCROLL_THUMBTRACK, wxCommandEventHandler(MyApp::control_cb));
	statusText = new wxStaticText(frame, wxID_ANY, "Dataset Inactive", wxDefaultPosition, wxDefaultSize);
	browseText = new wxStaticText(frame, wxID_ANY, "", wxDefaultPosition, wxDefaultSize);
	browseText->Disable();

	glSizer->Add(sbH, 0, wxALIGN_LEFT);
	glSizer->Add(statusText, 0, wxALIGN_LEFT);
	glSizer->Add(browseText, 0, wxALIGN_LEFT);
	parentSizer->Add(glSizer, 0, wxALIGN_LEFT|wxALIGN_TOP);
	parentSizer->Add(sbV, 0, wxALIGN_LEFT);
	parentSizer->Add(sidePanel, 0, wxRIGHT|wxALIGN_RIGHT);

	sidePanel->SetSizer(vbox);
	sidePanel->SetAutoLayout(true);
	frame->SetSizer(parentSizer);
	frame->SetAutoLayout(true);
	
	frame->Show();
	parentFrame = frame;

	return true;
}

BEGIN_EVENT_TABLE(GLPane, wxGLCanvas)
//EVT_MOTION(GLPane::mouseMoved)
//EVT_LEFT_DOWN(GLPane::mouseDown)
//EVT_LEFT_UP(GLPane::mouseReleased)
//EVT_RIGHT_DOWN(GLPane::rightClick)
//EVT_LEAVE_WINDOW(GLPane::mouseLeftWindow)
EVT_SIZE(GLPane::resized)
EVT_KEY_DOWN(GLPane::keyPressed)
//EVT_KEY_UP(GLPane::keyReleased)
EVT_MOUSEWHEEL(GLPane::mouseWheelMoved)
EVT_PAINT(GLPane::render)
EVT_IDLE(GLPane::OnIdle)
END_EVENT_TABLE()

void FontAssign(void *font, const unsigned char *text, float x, float y)
{
	glRasterPos2f(x, y);

	while(*text != '\0')
	{
		glutBitmapCharacter( font, *text );
		++text;
	}
}

bool ProcessFile(SNDFILE *file, SF_INFO &sfinfo)
{
	unsigned int i = 0, cols = 1, rows = 0, tabbed = 0, comma = 0;

	//char sample[2], pRIFF[12], pFMT[24], pDATA[6];
	//file.Read(pRIFF,12);
	//file.Read(pFMT, 24);
	//file.Read(pDATA,6);
	//file.Seek(42);
	int channels = sfinfo.channels;
	int BUFFER_LEN = 1024*channels;
	int readCount;
	int readPointer = 0;
	// prepare a 3 seconds buffer and write it
	for (int chan = 0; chan < channels; chan++) {
		timeseries.push_back(vector<double>());
	}
	double data[BUFFER_LEN];
	// DSPAudioBuffer converts all multichannel files to mono by averaging the channels
	// This is probably not an optimal way to convert to mono
	float monoAverage;
	//timeseries[0].resize(BUFFER_LEN);
	/* While there are samples in the input file, read them, process
    	** them and write them to the output file.
    	*/
    	while ((readCount = sf_read_double(file, data, 1024)))
	{
		//continue;
		readPointer = 0;
		for (int k = 0; k < readCount; k++) {
			// for each frame...
			//monoAverage = 0;
			for(int j = 0; j < channels; j++) {
				timeseries[j].push_back(data[readPointer + j]);
			}
			//monoAverage /= channels;
			readPointer += channels;
			i++;
			//timeseries[0].push_back(monoAverage);
		}
		//double channel_gain[MAX_CHANNELS] = { 0.5, 0.8, 0.1, 0.4, 0.4, 0.9 } ;
		//int k, chan;
		//
		///* Process the data here. 
		//** If the soundfile contains more then 1 channel you need to take care of 
		//** the data interleaving youself.
		//** Current we just apply a channel dependant gain. 
		//*/
		//
		//for (chan = 0 ; chan < channels; chan ++) {
		//	for (k = chan ; k < i; k+= channels) {
		//	        data[k] *= channel_gain[chan];
		//	}
		//}
	}

	//file.write(&sample[0], size);
	//while(!file.Eof())
	//{
	//	file.Read(sample,10); 
	//        double samples=reinterpret_cast< char >(sample[1]);
	//	timeseries[0].push_back(samples);
	//	++i;
	//}

	dataSize = rows = i;
	maxArrays = numArrays = cols = channels;
	return true;
}

/* Reads each line from dataset file and
 * detects the delimiter such that each
 * column of data represents a separate signal
 */
bool ProcessFile(ifstream &file, bool header)
{
//	if (file.is_open())
//	{
//		string line;
//		while (getline(file, line, ' ')) {
//			
//		}
//	}
	unsigned int i = 0, cols = 1, rows = 0, tabbed = 0, comma = 0;
	//size_t* tabs = (size_t*)malloc(numArrays*sizeof(size_t));
	string line;
	
	if (dataType == "Audio") {
		//s_riff_hdr riff_hdr;
    		//s_chunk_hdr chunk_hdr;
    		//uint32_t padded_size;
    		//s_wavefmt *fmt = NULL;
		//timeseries.push_back(vector<double>());

    		//file.read(reinterpret_cast<char*>(&riff_hdr), sizeof(riff_hdr));
    		//if (!file) return false;

    		//if (memcmp(riff_hdr.id, "RIFF", 4) != 0) return false;

    		//cout << "size=" << riff_hdr.size << endl;
    		//cout << "type=" << string(riff_hdr.type, 4) << endl;

    		//if (memcmp(riff_hdr.type, "WAVE", 4) != 0) return false;

    		//// chunks can be in any order!
    		//// there is no guarantee that "fmt" is the first chunk.
    		//// there is no guarantee that "fmt" is immediately followed by "data".
    		//// There can be other chunks present!

    		//do
    		//{
		//	file.read(reinterpret_cast<char*>(&chunk_hdr), sizeof(chunk_hdr));
    		//    	if (!file) return false;

    		//    	padded_size = ((chunk_hdr.size + 1) & ~1);

    		//    	cout << "id=" << string(chunk_hdr.id, 4) << endl;
    		//    	cout << "size=" << chunk_hdr.size << endl;
    		//    	cout << "padded size=" << padded_size << endl;

    		//    	if (memcmp(chunk_hdr.id, "fmt ", 4) == 0) 
    		//    	{
		//		if (chunk_hdr.size < sizeof(s_wavefmt)) return false;

    		//    	    	timeseries[0].resize(padded_size);

    		//    	    	file.read(reinterpret_cast<char*>(&timeseries[0]), padded_size);
    		//    	    	if (!file) return false;

    		//    	    	fmt = reinterpret_cast<s_wavefmt*>(&timeseries[0]);

    		//    	    	cout << "format_tag=" << fmt->format_tag << endl;
    		//    	    	cout << "channels=" << fmt->channels << endl;
    		//    	    	cout << "sample_rate=" << fmt->sample_rate << endl;
    		//    	    	cout << "avg_bytes_sec=" << fmt->avg_bytes_sec << endl;
    		//    	    	cout << "block_align=" << fmt->block_align << endl;

    		//    	    	if (fmt->format_tag == 1) // PCM
    		//    	    	{
		//			//if (chunk_hdr.size < sizeof(s_pcmwavefmt)) return false;

    		//    	    	    	s_pcmwavefmt *pcm_fmt = reinterpret_cast<s_pcmwavefmt*>(fmt);

    		//    	    	    	cout << "bits_per_sample=" << pcm_fmt->bits_per_sample << endl;
    		//    	    	}
    		//    	    	else
    		//    	    	{
		//			if (chunk_hdr.size < sizeof(s_wavefmtex)) return false;

    		//    	    	    	s_wavefmtex *fmt_ex = reinterpret_cast<s_wavefmtex*>(fmt);

    		//    	    	    	cout << "bits_per_sample=" << fmt_ex->bits_per_sample << endl;
    		//    	    	    	cout << "extra_size=" << fmt_ex->extra_size << endl;

    		//    	    	    	if (fmt_ex->extra_size != 0)
    		//    	    	    	{
		//				if (chunk_hdr.size < (sizeof(s_wavefmtex) + fmt_ex->extra_size)) return false;

    		//    	    	    	    	uint8_t *extra_data = reinterpret_cast<uint8_t*>(fmt_ex + 1);
    		//    	    	    	    	// use extra_data, up to extra_size bytes, as needed...
    		//    	    	    	}
    		//    	    	}
    		//    	}
    		//    	else if (memcmp(chunk_hdr.id, "data", 4) == 0)
    		//    	{
		//		// process chunk data, according to fmt, as needed...

    		//    	    	file.ignore(padded_size);
    		//    	    	if (!file) return false;
    		//    	}
    		//    	else
    		//    	{
		//		// process other chunks as needed...

    		//    	    	file.ignore(padded_size);
    		//    	    	if (!file) return false;
    		//    	}
    		//}while (!file.eof());
		//double compression_rate;
		//file.read((char*)&compression_rate, sizeof(compression_rate));
		//cout << "Compression Rate: " << compression_rate << endl;
		//
		//// As well as probably the sample rate...
		//double sample_rate;
		//file.read((char*)&sample_rate, sizeof(sample_rate));
		//cout << "Sample Rate: " << sample_rate << endl;
		//
		//int nb_samples;
		//file.read((char*)&nb_samples, sizeof(nb_samples));
		//cout << "Samples: " << nb_samples << endl;
		//
		//timeseries[0].resize(nb_samples);
		//timeseries[0].assign(istreambuf_iterator<char>(file),
		//		istreambuf_iterator<char>());
		//file.read((char*)&timeseries[0], nb_samples * sizeof(double));
	}
	else {
		while(getline(file, line)) {
			stringstream linestream(line);
			string data;

			if (i == 0)
			{
				if (line.find_first_of('\t') != string::npos) {
					tabbed = 1;
					comma = 0;
					cols += count(line.begin(), line.end(), '\t');
				}
				else if (line.find_first_of(",") != string::npos) {
					tabbed = 0;
					comma = 1;
					cols += count(line.begin(), line.end(), ',');
				}
				else {
					tabbed = 0;
					comma = 0;
				}
				if (header) { // Process header line (if any)
					while (getline(linestream, data, ',')) {
						data.erase(remove(data.begin(), data.end(), ' '), data.end());
						signalLabels.push_back(data);
					}
					++i;
					continue;
				}
			}
		
			if (tabbed) {
				for (int k = 0; k < cols; k++) {
					if (timeseries.size() != cols) {
						timeseries.push_back(vector<double>());
					}
					getline(linestream, data, '\t');
					timeseries[k].push_back(wxAtof(data));
				}
			}
			else if (comma) {
				for (int k = 0; k < cols; k++) {
					if (timeseries.size() != cols) {
						timeseries.push_back(vector<double>());
					}
					getline(linestream, data, ',');
					timeseries[k].push_back(wxAtof(data));
				}
			}
			else {
				for (int k = 0; k < cols; k++) {
					if (timeseries.size() != cols) {
						timeseries.push_back(vector<double>());
					}
					getline(linestream, data);
					timeseries[k].push_back(wxAtof(data));
				}
			}

			++i;
			++rows;
			
			//char line[BUFSIZ]; // space to read a line into
			//while(fgets(line, sizeof line, file) && i < limit) // read each line
			//{
			//	char *token = line; // point to the beginning of the line
			//	if (header && i == 0) { // Process header line (if any)
			//		++i;
			//		continue;
			//	}
			//	
			//	while(*token) {// loop through each character on the line
			//		// search for delimiters
			//		size_t len = strcspn(token, "\n");
			//		size_t index = 0;
			//		for (t = 0; t <= len; t++) {
			//			if (token[t] >= 65 && token[t] <= 122 && token[t] != 101) {
			//				notNumbers++;
			//			}
			//			if (token[t] == '\t' || token[t] == ',' || token[t] == '\n') {
			//				tabs[index] = t;
			//				index++;
			//			}
			//		}
			//		for (k = 0; k < maxArrays && notNumbers == 0; k++) {
			//			char *temp = (char*)malloc(6);
			//			char *pnew = (char*)malloc(tabs[k]+1);
			//			if (k == 0) {
			//				//temp = substr(token, 0, tabs[k]);
			//				temp = strncpy((char*)pnew, (char*)token, tabs[k]);
			//				temp[tabs[k]] = '\0';
			//			}
			//			else {
			//				//temp = substr(token, tabs[k-1], tabs[k]);
			//				temp = strncpy((char*)pnew, (char*)token + tabs[k-1], tabs[k]);
			//			}
			//			// Assign the value that the pointer points to in memory to each slot in the array
			//			//sscanf_s(temp, "%lf", &arr[k][i]);
			//			if (timeseries.size()!=maxArrays) {
			//				timeseries.push_back(vector<double>());
			//			}
			//			timeseries[k].push_back(atof(temp));
			//		}
			//		notNumbers = 0;
			//		// advance pointer by one more than the length of the found text
			//		token += len + 1;
			//	}
			//	++i;
			//}
			//// Close the file
			//fclose(file);
		}
	}

	dataSize = rows;
	maxArrays = numArrays = cols;

	return true;
}

void CountLines(FILE *tempFile, bool header)
{
	unsigned int i = 0, k = 0, notNumbers = 0;
	int size = 0, tabs = 0, checked = 0;

	if(tempFile) {
		char line[BUFSIZ]; // space to read a line into
		while(fgets(line, sizeof(line), tempFile)) // read each line
		{
			char *token = line; // point to the beginning of the line
			if (header && i == 0)
			{
				string tok;
				stringstream sline(line);
				while (getline(sline, tok, ','))
				{
					if (tok[0] == ' ') {
						tok = tok.substr(1,tok.length());
					}
					signalLabels.push_back(tok);
				}
				++i;
				continue;
			}
			
			while(*token) {// loop through each character on the line
				// search for delimiters
				size_t len = strcspn(token, "\n");
				if (checked == 0) {
					for (k = 0; k <= len; k++) {
						if (token[k] >= 65 && token[k] <= 122 && token[k] != 101) {
							notNumbers++;
						}
						if(token[k] == '\t' || token[k] == ',' || token[k] == '\n') {
							tabs++;
							checked = 1;
						}
					}
				}
				if (strcspn(token,"\n") && notNumbers == 0) {
					size++;
				}
				notNumbers = 0;
				// advance pointer by one more than the length of the found text
				token += len + 1;
			}
			++i;
		}
		// Close the file
		fclose(tempFile);
	}

	dataSize = size;
	maxArrays = numArrays = tabs;
}

double euclideanDistance(const vector<Point> &pattern, const vector<Clusters> &centroid) {
	float distance, diff, sum = 0.0f;

	for (unsigned int i = 0; i < pattern.size(); i++) {
		diff = pattern[i].y - centroid[i].y;
		sum += pow(diff, 2);
	}

	distance = sqrt(sum);
	return distance;
}

const vector<Clusters>& reComputeClusterCenters(const vector< vector<Point> > &points, int numClusters, vector<Clusters> &centers, int jMax, int W, int L, int i) {
	int flags = 0;
	
	vector<double> newClusterCenter;
	
	for (int k = 0; k < W; k++) {
		double sumX = 0, sumY = 0;
		centers[i].totalMembers = 0;
		for (int j = 0; j < jMax; j++) {
			if (points[j][k].clusterIndex == i) {
				//sumX += points[j][k].x;
				sumY += points[j][k].y;
				centers[i].totalMembers++;
			}
		}
		if (sumY != 0)
			newClusterCenter.push_back(sumY/centers[i].totalMembers);
		else
			newClusterCenter.push_back(0);
	}

	for (int k = 0; k < W; k++) {
		double oldY = centers[k].y;
		//centers[k].x = sumX/centers[i].totalMembers;
		centers[k].y = newClusterCenter[k];
		if (oldY == centers[k].y) {
			flags++;
		}
	}

	if (i == 0) {
		movement = 0;
	}

	// If the clusters no longer move
	if (flags == W) {
		movement++;
	}

	for (int k = 0; k < W; k++) {
		newClusterCenter.erase(newClusterCenter.begin());
	}

	return centers;
}

void BuildLists()
{
	int i, N = dataSize;
	unsigned int k = 0;
	orig = (int*)malloc(numArrays*sizeof(int));
	orig[0] = glGenLists(20);

	for(k = 0; k < maxArrays; k++) {
		glNewList(orig[k], GL_COMPILE);
		glBegin(GL_LINE_STRIP);	
			for(i = 0; i < N; i++)
			{
				glVertex2f(i, -timeseries[k][i]);
				if (-timeseries[k][i] < maxOrig) {
					maxOrig = -timeseries[k][i];
				}
				if (-timeseries[k][i] > minOrig) {
					minOrig = -timeseries[k][i];
				}
			}
		glEnd();
		glEndList();
		if(k!=(numArrays-1)) orig[k+1] = orig[k]+1;
	}

	graphColourLabel = orig[numArrays-1]+1;

	glNewList(graphColourLabel, GL_COMPILE);
	glBegin(GL_QUADS);
		glVertex2f(0.0f, 0.0f);
		glVertex2f(0.0f, 1.0f);
		glVertex2f(1.0f, 1.0f);
		glVertex2f(1.0f, 0.0f);
	glEnd();
	glEndList();

	xaxis = graphColourLabel+1;

	glNewList(xaxis, GL_COMPILE);
	glBegin(GL_LINES);
		glVertex2f(0.0f, 0.0f);
		glVertex2f(N*0.8f, 0.0f);
		for (i = 0; i < (N/(windowWidth*2))+1; i++) {
			glVertex2f(((windowWidth*2)/1.25)*i, 0.0f);// Labels every W*2 units
			glVertex2f(((windowWidth*2)/1.25)*i, 10.0f);
		}
	glEnd();
	glEndList();

	yaxis = xaxis+1;

	glNewList(yaxis, GL_COMPILE);
	glBegin(GL_LINES);
		double round = ceil(-maxOrig);
		double round2 = ceil(-minOrig);
		glVertex2f(0.0f, -round2);
		glVertex2f(0.0f, -round);
		for (i = round2; i <= round; i++) {
			glVertex2f(0.0f, -i);
			glVertex2f(-5.0f, -i);
		}
		//glVertex2f(0.0f, -7.7f);// Label for 5
		//glVertex2f(-5.0f, -7.7f);
		//glVertex2f(0.0f, -11.1f);// Label for 10
		//glVertex2f(-5.0f, -11.1f);
	glEnd();
	glEndList();

	listBuilt = 1;
}

void draw()
{
	unsigned char* label = (unsigned char*)malloc(26);
	unsigned int unit = 0;
	double round = ceil(-maxOrig);
	double round2 = ceil(-minOrig);
	switch (view) {
		case 0: // Legend
			glViewport(0, 0, GL_WIDTH, GL_HEIGHT);
			glScissor(0, 0, GL_WIDTH, GL_HEIGHT);
			glEnable(GL_SCISSOR_TEST);
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
			glDisable(GL_SCISSOR_TEST);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, GL_WIDTH, GL_HEIGHT, 1, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glColor3f(1.0f, 1.0f, 1.0f);

			if (origCheck) {
				glPushMatrix();
					glScalef(10.0f, 10.0f, 1.0f);
					glTranslatef(110.0f, 1.0f, 0.0f);

					glColor3f(0.0f, 0.0f, 1.0f);
					glCallList(graphColourLabel);

					sprintf((char*)label, "Original Time Series");
					FontAssign(GLUT_BITMAP_HELVETICA_12, label, 1.5f, 1.0f);
				glPopMatrix();
			}

			if (dctCheck) {
				glPushMatrix();
					glScalef(10.0f, 10.0f, 1.0f);
					glTranslatef(110.0f, 4.0f, 0.0f);

					glColor3f(1.0f, 0.0f, 0.0f);
					glCallList(graphColourLabel);
					sprintf((char*)label, "DCT Transformed");
					FontAssign(GLUT_BITMAP_HELVETICA_12, label, 1.5f, 1.0f);
				glPopMatrix();
			}

			if (inverseCheck) {
				glPushMatrix();
					glScalef(10.0f, 10.0f, 1.0f);
					glTranslatef(110.0f, 7.0f, 0.0f);

					glColor3f(0.0f, 1.0f, 0.0f);
					glCallList(graphColourLabel);
					sprintf((char*)label, "Inverse DCT");
					FontAssign(GLUT_BITMAP_HELVETICA_12, label, 1.5f, 1.0f);
				glPopMatrix();
			}

			if (motifsCheck) {
				glPushMatrix();
					glScalef(10.0f, 10.0f, 1.0f);
					glTranslatef(110.0f, 10.0f, 0.0f);

					glColor3f(1.0f, 0.0f, 1.0f);
					glCallList(graphColourLabel);
					sprintf((char*)label, "Motifs");
					FontAssign(GLUT_BITMAP_HELVETICA_12, label, 1.5f, 1.0f);
				glPopMatrix();
			}

			if (discreteCheck) {
				glPushMatrix();
					glScalef(10.0f, 10.0f, 1.0f);
					glTranslatef(110.0f, 13.0f, 0.0f);

					glColor3f(1.0f, 1.0f, 0.0f);
					glCallList(graphColourLabel);
					sprintf((char*)label, "Discrete Time Series");
					FontAssign(GLUT_BITMAP_HELVETICA_12, label, 1.5f, 1.0f);
				glPopMatrix();
			}

		case 1: // signals
			glViewport(40, 0.0f, GL_WIDTH-40, GL_HEIGHT);
			glScissor(40, 0.0f, GL_WIDTH-40, GL_HEIGHT);
			glEnable(GL_SCISSOR_TEST);
			glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
			glClear(GL_DEPTH_BUFFER_BIT);
			glDisable(GL_SCISSOR_TEST);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(40, GL_WIDTH-40, 10.0f, 0.0f, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			if (!listBuilt) {
				BuildLists();
			}

			if (origCheck) {
				glPushMatrix(); /* GL_MODELVIEW is default */
					glScalef(1.0f/1.25f, scaleY+0.2, 1.0f);
					glTranslatef(offsetX+50.0f, offsetY+35.0f, 0.0f);
					glColor3f(0.0f, 0.0f, 1.0f);
					
					if (listBuilt) {
						if (numArrays == maxArrays) {
							for(int i = 0; i < numArrays; i++) {
								glCallList(orig[i]);
							}
						}
						else {
							glCallList(orig[numArrays]);
						}
					}
				glPopMatrix();
			}

			if (dctCheck) {
				glPushMatrix();
					glScalef(1.0f/1.25f, scaleY+0.2, 1.0f);
					glTranslatef((offsetX+50.0f), offsetY+35.0f, 0.0f);
					glColor3f(1.0f, 0.0f, 0.0f);
					if (numArrays == maxArrays) {
						for(int i = 0; i < numArrays; i++) {
							if (blocksCreated) glCallList(DCTGraph[i]);
						}
					}
					else {
						if (blocksCreated) glCallList(DCTGraph[numArrays]);
					}
				glPopMatrix();
			}

			if (inverseCheck) {
				glPushMatrix();
					glScalef(1.0f/1.25f, scaleY+0.2, 1.0f);
					glTranslatef((offsetX+50.0f), offsetY+35.0f, 0.0f);
					glColor3f(0.0f, 1.0f, 0.0f);
					if (numArrays == maxArrays) {
						for(int i = 0; i < numArrays; i++) {
							if (blocksCreated) glCallList(idctGraph[i]);
						}
					}
					else {
						if (blocksCreated) glCallList(idctGraph[numArrays]);
					}
				glPopMatrix();
			}

			glPushMatrix();
				glScalef(1.0f/1.25f, scaleY+0.2, 1.0f);
				glTranslatef((offsetX+50.0f), offsetY+35.0f, 0.0f);
				glColor3f(0.0f, 1.0f, 1.0f);
				if (numArrays == maxArrays) {
					for(int i = 0; i < numArrays; i++) {
						if (blocksCreated) glCallList(clusterCentroids[i]);
					}
				}
				else {
					if (blocksCreated) glCallList(clusterCentroids[numArrays]);
				}
			glPopMatrix();

			if (motifsCheck) {
				glPushMatrix();
					glScalef(1.0f/1.25f, scaleY+0.2, 1.0f);
					glTranslatef((offsetX+50.0f), offsetY+35.0f, 0.0f);
					glColor3f(1.0f, 0.0f, 1.0f);
					if (numArrays == maxArrays) {
						for(int i = 0; i < numArrays; i++) {
							for (int j = 0; j < numClusters; j++) {
								if (blocksCreated) glCallList(motifs[i][j]);
							}
						}
					}
					else {
						for (int j = 0; j < numClusters; j++) {
							if (blocksCreated) glCallList(motifs[numArrays][j]);
						}
					}
				glPopMatrix();
			}

			if (discreteCheck) {
				glPushMatrix();
					glScalef(1.0f/1.25f, scaleY+0.2, 1.0f);
					glTranslatef((offsetX+50.0f), offsetY+35.0f, 0.0f);
					glColor3f(1.0f, 1.0f, 0.0f);
					if (numArrays == maxArrays) {
						for(int i = 0; i < numArrays; i++) {
							if (blocksCreated) glCallList(discreteSeries[i]);
						}
					}
					else {
						if (blocksCreated) glCallList(discreteSeries[numArrays]);
					}
				glPopMatrix();
			}
		
		case 2: //x-axis
			glViewport(40, 0, GL_WIDTH-40, GL_HEIGHT);
			glScissor(40, 0, GL_WIDTH-40, GL_HEIGHT);
			glEnable(GL_SCISSOR_TEST);
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
			glDisable(GL_SCISSOR_TEST);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(40, GL_WIDTH-40, GL_HEIGHT, 1, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glColor3f(1.0f, 1.0f, 1.0f);

			glPushMatrix();
				glTranslatef(offsetX*0.8f+40.0f, 500.15f, 0.0f);
				glColor3f(1.0f, 1.0f, 1.0f);
				glCallList(xaxis);
				unit = 0;
				for (unsigned int i = 0; i < (dataSize/(windowWidth*2))+1; i++) {
					sprintf((char*)label, "%d", unit);
					FontAssign(GLUT_BITMAP_HELVETICA_10, label, (i<500) ? ((windowWidth*2)/1.25)*i : ((windowWidth*2)/1.25)*i-30.0f, 20.0f);
					unit+=(windowWidth*2);
				}
			glPopMatrix();

			if (motifsCheck) {
				glPushMatrix();
					glTranslatef(offsetX*0.8f+40.0f, 500.15f, 0.0f);
					glColor3f(1.0f, 0.0f, 1.0f);
					unit = 0;
					for (int i = 0; i < 6; i++) {
						sprintf((char*)label, "%d", unit);
						FontAssign(GLUT_BITMAP_HELVETICA_10, label, i*(0.95*windowWidth), 40.0f);
						unit+=(windowWidth/5);
					}
				glPopMatrix();
			}
		
		case 3: // y-axis
			glViewport(0, 0, GL_WIDTH, GL_HEIGHT);
			glScissor(0, 0, GL_WIDTH, GL_HEIGHT);
			glEnable(GL_SCISSOR_TEST);
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
			glDisable(GL_SCISSOR_TEST);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, GL_WIDTH, 10.0f, 0.0f, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			glPushMatrix();
				glScalef(1.0f, 0.2, 1.0f);
				glTranslatef(40.0f, offsetY+35.0f, 0.0f);
				glColor3f(1.0f, 1.0f, 1.0f);
				glCallList(yaxis);
				//sprintf_s((char*)label, sizeof(label), "%d", unit);
				//FontAssign(GLUT_BITMAP_HELVETICA_10, label, -12.0f, -4.2f);
				if (round > 0) {
					for (int i = round2; i <= round; i++) {
						sprintf((char*)label, "%d", i);
						if (i >= 0) {
							FontAssign(GLUT_BITMAP_HELVETICA_10, label, -14.0f+((((i==0)?1:numdigits(i))-1)*-4), 0.25f-i);
						}
						else {
							FontAssign(GLUT_BITMAP_HELVETICA_10, label, -20.0f+((((i==0)?1:numdigits(i))-1)*-4), 0.25f-i);
						}
					}
				}
				//sprintf_s((char*)label, sizeof(label), "%d", unit+=5);
				//Font(GLUT_BITMAP_HELVETICA_10, label, -18.0f, -10.9f);
			glPopMatrix();

			if (discreteCheck) {
				glPushMatrix();
					glScalef(1.0f, scaleY+0.2f, 1.0f);
					glTranslatef(40.0f, offsetY+35.0f, 0.0f);
					glColor3f(1.0f, 1.0f, 0.0f);
					unsigned char letter = 'A';
					for (int i = 0; i < numClusters; i++) {
						sprintf((char*)label, "%c", letter+i);
						FontAssign(GLUT_BITMAP_HELVETICA_10, label, -30.0f, i*-5.0f);
					}
				glPopMatrix();
			}
	}

	//glutPostRedisplay();

	free(label);
};

template<typename Signal>
void ruleMining(vector<string> &alphabet, int TimeUnits, Signal signal)
{
	// Frequency
	vector<string> uniqueAlpha;
	for (vector<string>::iterator alphaIter = alphabet.begin(); alphaIter != alphabet.end(); ++alphaIter) {
		int frequency = 0;
		bool freqExists = false, ruleExists = false;
		if (alphaIter == alphabet.begin()) {
			uniqueAlpha.push_back(*alphaIter);
		}
		for (unsigned int j = 0; j < uniqueAlpha.size(); j++) {
			if (*alphaIter == uniqueAlpha[j]) {
				freqExists = true;
			}
		}
		for (vector<string>::iterator alphaIter2 = alphabet.begin(); alphaIter2 != alphabet.end() && (!freqExists || alphaIter == alphabet.begin()); ++alphaIter2) {
			if (*alphaIter == *alphaIter2) {
				frequency++;
			}
		}
		if (!freqExists) {
			uniqueAlpha.push_back(*alphaIter);
			symbolFreq.insert(make_pair(*alphaIter, frequency));
		}
		else if (alphaIter == alphabet.begin()) {
			symbolFreq.insert(make_pair(*alphaIter, frequency));
		}

		// Rule Frequency
		int digits = numdigits(TimeUnits);
		if (digits >= 2) {
			digits = pow(10.0, (digits-1));
		}
		else {
			digits = 1;
		}

		string nextSymbol = (alphaIter+(TimeUnits/digits)) >= alphabet.end() ? "" : *(alphaIter+(TimeUnits/digits));
		if (!nextSymbol.empty()) {
			Keys *currKey = new Keys(*alphaIter, nextSymbol, TimeUnits);
			for (map<Keys*,double>::iterator ruleIter = ruleFreq.begin(); ruleIter != ruleFreq.end(); ++ruleIter) {
				if (*currKey == *(*ruleIter).first) {
					ruleExists = true;
					(*ruleIter).second++;
				}
			}
			if (!ruleExists) {
				ruleFreq.insert(make_pair(currKey,1));
			}
		}
	}
	
	// Relative Frequency
	for (map<string,double>::iterator it = symbolFreq.begin(); it != symbolFreq.end(); ++it) {
		symbolRelFreq.insert(make_pair(it->first, it->second/alphabet.size()));
	}

	// Rule Confidence
	for (map<Keys*,double>::iterator ruleIter = ruleFreq.begin(); ruleIter != ruleFreq.end(); ++ruleIter) {
		map<string,double>::iterator symbolIter = symbolFreq.find((*ruleIter).first->key1);
		ruleConf.insert(make_pair((*ruleIter).first, 
			((*ruleIter).second)/(*symbolIter).second));
	}

	multimap<double, Keys*> results = invert_map(ruleConf);

	// Print to GUI
	unsigned int rLines = rulesList->GetItemCount();
	unsigned int fLines = frequencyList->GetItemCount();
	map<string,double>::iterator it = symbolFreq.begin();

	for (unsigned int i = fLines; i < (fLines+symbolFreq.size()) && !freqDisplayed; i++) {
		stringstream signalText, patternText, freqText;
		signalText << signal;
		patternText << (*it).first;
		freqText << (*it).second;
		long index = frequencyList->InsertItem(i, signalText.str());
		frequencyList->SetItem(index, 1, patternText.str());
		frequencyList->SetItem(index, 2, freqText.str());
		++it;
	}

	multimap<double,Keys*>::reverse_iterator ruleConfIter = results.rbegin();
	for (unsigned int i = rLines; i < (rLines+results.size()); i++) {
		stringstream signalText, ruleText, confText, confidence;
		confidence.precision(4);
		confidence << (*ruleConfIter).first*100;
		signalText << signal;
		ruleText << (*ruleConfIter).second->key1 << " ->" << (*ruleConfIter).second->timeUnits << " "
			<< (*ruleConfIter).second->key2;
		confText << confidence.str() << "%";
		long index = rulesList->InsertItem(i, signalText.str());
		rulesList->SetItem(index, 1, ruleText.str());
		rulesList->SetItem(index, 2, confText.str());
		++ruleConfIter;
	}

	while (!uniqueAlpha.empty()) {
		uniqueAlpha.erase(uniqueAlpha.begin());
		symbolFreq.erase(symbolFreq.begin());
		symbolRelFreq.erase(symbolRelFreq.begin());
	}
	
	while (!ruleFreq.empty()) {
		ruleFreq.erase(ruleFreq.begin());
		ruleConf.erase(ruleConf.begin());
		results.erase(results.begin());
	}

	rulesList->SetColumnWidth(0, wxLIST_AUTOSIZE_USEHEADER);
	rulesList->SetColumnWidth(1, wxLIST_AUTOSIZE);
	rulesList->SetColumnWidth(2, wxLIST_AUTOSIZE_USEHEADER);

	for (int i = 0; i < frequencyList->GetColumnCount(); i++) {
		frequencyList->SetColumnWidth(i, wxLIST_AUTOSIZE_USEHEADER);
	}
}

void Discrete(int W, int L, int M)
{
	int N = dataSize, jMax = ((N-W)/L)+1;
	double sum = 0, coeff = 0;// The DCT CoEfficients
	vector< vector< vector<Point> > > points;
	vector< vector< vector<Point> > > points2;
	vector< vector< vector<Clusters> > > cluster;
	vector< vector< vector<Point> > > centroidPatterns;
	
	DCTGraph = (int*)malloc(maxArrays*sizeof(int));
	DCTGraph[0] = yaxis+1;
	for (unsigned int a = 0; a < maxArrays; a++) {
		glNewList(DCTGraph[a], GL_COMPILE);
		glBegin(GL_LINE_STRIP);
			points.push_back(vector< vector<Point> >());
			// Loop through all blocks/subsequences
			for (int j = 1; j <= jMax; j++) {
				vector<Point> DCTPoints;
				// Loop through each point for a given block from WMin to WMax
				for (int k = (L*(j-1)); k < (L*(j-1)+W); k++) {
					int u = 0;
					// Make sure each block is from WMin to WMax long
					// If it is below WMax and after the first block, make k start at 0
					if (j > 1 && k >= L) {
						u = (k-(L*(j-1)));
					}
					else {
						u = k;
					}

					Point point;
					point.x = k;

					if ((k-(L*(j-1))) >= M) {
						point.y = 0;// If after the first M coefficients don't bother calculating DCT so set to 0
					}
					else {// If within the first M coefficients of the current subsequence
						if (k == (L*(j-1))) {
							coeff = 1/sqrt((double)W);
						}
						else {
							coeff = sqrt(2.0/W);
						}

						// Apply the DCT from t=0 to W-1
						for (int t = 0; t < W; t++) {
							// Calculate the DCT for each subsequence
							sum += timeseries[a][t+(L*(j-1))] * cos(((2*t+1) * (u) * M_PI)/(2*W));
						}
					
						// Set DCT values to array
						point.y = coeff * sum;// Assign the DCT for the current block
					}
					DCTPoints.push_back(point);
					// Reset sum to 0 for next loop
					sum = 0.0f;
				
					glVertex2f(DCTPoints[u].x, -DCTPoints[u].y);
				}
				points[a].push_back(DCTPoints);
				DCTPoints.clear();
			}
		glEnd();
		glEndList();
		if(a!=(maxArrays-1)) DCTGraph[a+1] = DCTGraph[a]+1;
	}
	
	idctGraph = (int*)malloc(maxArrays*sizeof(int));
	idctGraph[0] = DCTGraph[maxArrays-1]+1;
	for (unsigned int a = 0; a < maxArrays; a++) {
		glNewList(idctGraph[a], GL_COMPILE);
		glBegin(GL_LINE_STRIP);
			points2.push_back(vector< vector<Point> >());
			// Loop through all blocks/subsequences
			for (int j = 1; j <= jMax; j++) {
				vector<Point> IDCTPoints;
				// Loop through each point for a given block from WMin to WMax
				for (int k = (L*(j-1)); k < (L*(j-1)+W); k++) {
					int t = 0;
					// Make sure each block is from WMin to WMax long
					// If it is below WMax and after the first block, make k start at 0
					if (j > 1 && k >= L) {
						t = (k-(L*(j-1)));
					}
					else {
						t = k;
					}

					// Apply the DCT from t=0 to W-1
					for (int u = 0; u < W; u++) {
						if (u == 0) {
							coeff = 1/sqrt((double)W);
						}
						else {
							coeff = sqrt(2.0/W);
						}
						// Calculate the DCT for each subsequence
						sum += coeff * points[a][j-1][u].y * cos(((2*t+1) * (u) * M_PI)/(2*W));
					}
					Point point;
					// Set DCT values to array
					point.x = k;
					point.y = sum;// Assign the DCT for the current block
					IDCTPoints.push_back(point);
					// Reset sum to 0 for next loop
					sum = 0.0f;
				
					glVertex2f(IDCTPoints[t].x, -IDCTPoints[t].y);
				}
				points2[a].push_back(IDCTPoints);
				IDCTPoints.clear();
			}
		glEnd();
		glEndList();
		if (a!=(maxArrays-1)) idctGraph[a+1] = idctGraph[a]+1;
	}

	clusterCentroids = (int*)malloc(maxArrays*sizeof(int));
	clusterCentroids[0] = idctGraph[maxArrays-1]+1;
	for (unsigned int a = 0; a < maxArrays; a++) {
		glNewList(clusterCentroids[a], GL_COMPILE);
		glBegin(GL_LINE_STRIP);
			cluster.push_back(vector< vector<Clusters> >());
			// Loop through all blocks/subsequences
			for (int i = 0; i < numClusters; i++) {
				vector<Clusters> tempClusterCentroids;
				if (i == 0) {
					// This will be the first initial cluster centroid
					for (int k = 0; k < W; k++) {
						Clusters clusterCentroid;
						clusterCentroid.x = k;
						clusterCentroid.y = points[a][i][k].y;
						tempClusterCentroids.push_back(clusterCentroid);
					}
				}
				else {
					double max = 0;
					for (int j = i; j < jMax; j++) {
						double distance = 0;
						distance = euclideanDistance(points[a][j], cluster[a][i-1]);
						if (distance > max) {
							max = distance;
							for (int k = 0; k < W; k++) {
								Clusters clusterCentroid;
								clusterCentroid.x = k;
								clusterCentroid.y = points[a][j][k].y;
								if (tempClusterCentroids.size() >= (unsigned int)W) {
									tempClusterCentroids[k] = clusterCentroid;
								}
								else {
									tempClusterCentroids.push_back(clusterCentroid);
								}
							}
						}
					}
				}

				cluster[a].push_back(tempClusterCentroids);
				tempClusterCentroids.clear();
			}
			int run = 0;
			movement = 0;
			// K-Means Algorithm
			while (movement < numClusters) {
				// Assign each subsequence to the closest cluster
				// Loop through all blocks/subsequences
				for (int j = 1; j <= jMax; j++) {
					double distance = 0, min = N;
					for (int i = 0; i < numClusters; i++) {
						distance = euclideanDistance(points[a][j-1], cluster[a][i]);
						if (distance < min) {
							min = distance;
							for (int k = 0; k < W; k++) {
								points[a][j-1][k].clusterIndex = i;
							}
						}
					}
				}

				// Re-Compute cluster centroids by finding the mean of all points in the cluster
				for (int i = 0; i < numClusters; i++) {
					// Recompute cluster centroids for each cluster given the current time series
					cluster[a][i] = reComputeClusterCenters(points[a], numClusters, cluster[a][i], jMax, W, L, i);
				}
				run++;
			}
			centroidPatterns.push_back(vector< vector<Point> >());
			for (int i = 1; i <= numClusters; i++) {
				vector<Point> tempCentroidPatterns;
				// Loop through each point for a given block from WMin to WMax
				for (int k = (L*i); k < (L*i+W); k++) {
					int t = (k-(L*i));

					// Apply the DCT from t=0 to W-1
					for (int u = 0; u < W; u++) {
						if (u == 0) {
							coeff = 1/sqrt((double)W);
						}
						else {
							coeff = sqrt(2.0/W);
						}
						// Calculate the DCT for each subsequence
						sum += coeff * cluster[a][i-1][u].y * cos(((2*t+1) * (u) * M_PI)/(2*W));
					}
					Point point;
					// Set DCT values to array
					point.x = cluster[a][i-1][t].x;
					point.y = sum;// Assign the DCT for the current block
					// Reset sum to 0 for next loop
					sum = 0.0f;
					tempCentroidPatterns.push_back(point);
				}
				centroidPatterns[a].push_back(tempCentroidPatterns);
				tempCentroidPatterns.clear();
			}
		glEnd();
		glEndList();
		if (a!=(maxArrays-1)) clusterCentroids[a+1] = clusterCentroids[a]+1;
	}

	motifs = (int**)malloc(maxArrays*sizeof(int*));
	motifs[0] = (int*)malloc(numClusters*sizeof(int));
	motifs[0][0] = clusterCentroids[maxArrays-1]+1;
	for (unsigned int a = 0; a < maxArrays; a++) {
		for (int i = 0; i < numClusters; i++) {
			glNewList(motifs[a][i], GL_COMPILE);
			glBegin(GL_LINE_STRIP);
				for (int k = 0; k < W; k++) {
					centroidPatterns[a][i][k].x = k*6;
					glVertex2f(centroidPatterns[a][i][k].x, -centroidPatterns[a][i][k].y);
				}
			glEnd();
			glEndList();
			if (i!=(numClusters-1)) motifs[a][i+1] = motifs[a][i]+1;
		}
		if (a!=(maxArrays-1)) {
			motifs[a+1] = (int*)malloc(numClusters*sizeof(int));
			motifs[a+1][0] = motifs[a][numClusters-1]+1;
		}
	}

	discreteSeries = (int*)malloc(maxArrays*sizeof(int));
	discreteSeries[0] = motifs[maxArrays-1][numClusters-1]+1;
	for (unsigned int a = 0; a < maxArrays; a++) {
		clusterAlphabet.push_back(vector<string>());
		glNewList(discreteSeries[a], GL_COMPILE);
		glBegin(GL_LINE_STRIP);
			for (int j = 0; j < jMax; j++) {
				for (int i = 0; i < numClusters; i++) {
					if (points[a][j][0].clusterIndex == i) {
						glVertex2f((L*j), -i*5);
						glVertex2f((L*(j+1)), -i*5);
						char *symbol = (char*)malloc(1*sizeof(char));
						// 65 is char code for the letter A
						symbol[0] = 65+i;
						stringstream ss;
						ss << symbol[0];// Assign symbol to stringstream
						clusterAlphabet[a].push_back(ss.str());// push string to alphabet
					}
				}
			}
		glEnd();
		glEndList();
		if (a!=(maxArrays-1)) discreteSeries[a+1] = discreteSeries[a]+1;
	}

	// Free up memory
	for (unsigned int a = 0; a < maxArrays; a++) {
		points.erase(points.begin());
		points2.erase(points2.begin());
		cluster.erase(cluster.begin());
		centroidPatterns.erase(centroidPatterns.begin());
	}
	
	if (rulesList->GetItemCount() > 0) {
		rulesList->DeleteAllItems();
	}
	if (frequencyList->GetItemCount() > 0) {
		frequencyList->DeleteAllItems();
	}

	for (unsigned int a = 0; a < maxArrays; a++) {
		freqDisplayed = false;
		for (vector<int>::iterator timeIter = inferenceTimePeriod.begin(); timeIter != inferenceTimePeriod.end(); ++timeIter) {
			if (signalLabels.empty()) {
				ruleMining(clusterAlphabet[a],(*timeIter), a+1);
			}
			else {
				ruleMining(clusterAlphabet[a],(*timeIter),signalLabels[a]);
			}
			freqDisplayed = true;
		}
	}
	
	rulesList->RefreshItems(0, rulesList->GetItemCount()-1);
	frequencyList->RefreshItems(0, frequencyList->GetItemCount()-1);

	// The subsequences have been created
	blocksCreated = 1;
}

void plotGraph()
{
	if (!filename.empty() && dataSize != 0) {
		if (!blocksCreated) {
			wxBusyCursor wait;
			// Preprocess signal or signals based on the context of the dataset type
			if (normalise && !isNormalised) {
				timeseries = timeseries - transpose(tile(mean(timeseries), dataSize));
				timeseries /= transpose(tile(stdDev(timeseries), dataSize));
				isNormalised = true;
			}
			Discrete(windowWidth, slidingWinWidth, DCTSampleSize);
		}

		draw();
	}
}

GLPane::GLPane(wxFrame* parent, int* args) :
    wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxSize(GL_WIDTH, GL_HEIGHT), wxFULL_REPAINT_ON_RESIZE)
{
	int argc = 1;
	char* argv[1] = { wxString((wxTheApp->argv)[0]).char_str() };
	m_context = new wxGLContext(this);
	
	// To avoid flashing on MSW
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);
	glutInit(&argc, argv);
}

GLPane::~GLPane()
{
	delete m_context;
}

int GLPane::getWidth()
{
	return GetSize().x;
}
 
int GLPane::getHeight()
{
	return GetSize().y;
}

void GLPane::OnIdle(wxIdleEvent& evt)
{
	glClear(GL_COLOR_BUFFER_BIT);
	plotGraph();
	Refresh(false);

	glFlush();
	SwapBuffers();
	/* Force the redraw immediately, gets the gfx card to its max */
	evt.RequestMore();
}

/* Redrawing func */
void GLPane::render(wxPaintEvent& evt)
{
	if(!IsShown()) return;
	SetCurrent(*m_context);
	wxPaintDC dc(this); // only to be used in paint events. use wxClientDC to paint outside the paint event
	//wxGetApp().ProcessIdle();
};

void Init()
{
	isNormalised = false;
	ifstream file;
	/* A pointer to an SF_INFO stutct is passed to sf_open_read and sf_open_write
    	** which fill this struct with information about the file.
    	*/ 
    	SF_INFO sfinfo ; 
    	SNDFILE *audioFile;
	
	wxMessageDialog *dlg = new wxMessageDialog(parentFrame,
			wxT("Please choose a valid text file"), wxT("Error Opening File"),
			wxYES_NO | wxYES_DEFAULT | wxICON_ERROR);

	if (dataType == "Audio") {
		if (!(audioFile = sf_open(filename.c_str(), SFM_READ, &sfinfo))) /* Open failed so print an error message. */
		{
			cout << "Not able to open input file " << filename << endl;
        		/* Print the error message fron libsndfile. */
        		sf_perror(NULL);
			if (dlg->ShowModal() == wxID_NO) {
				dlg->EndModal(1);
				closeWindow();
			}
			else {
				dlg->EndModal(1);
				return;
			}
        	}
	}
	else {
		file.open(filename.c_str());
	}
	//FILE *temp = fopen(filename.c_str(), "r");
	// Opens the text file
	if(!file) {
		if (dlg->ShowModal() == wxID_NO) {
			//MessageBox(NULL, L"Please choose a valid text file.", L"Error Opening File", MB_RETRYCANCEL) == IDCANCEL
			dlg->EndModal(1);
			closeWindow();
		}
		else {
			dlg->EndModal(1);
			return;
		}
	}

	//int counterCheck = std::count(istreambuf_iterator<string>(temp), istreambuf_iterator<string>(), '\n');
	//int num_tabs = std::count(istreambuf_iterator<char>(temp), istreambuf_iterator<char>(), '\t');

	//CountLines(temp, header);
	//fclose(temp);

	//FILE *f = fopen(filename.c_str(), "rb");
	// Opens the text file
	//if(f == NULL) {
	//	if (dlg->ShowModal() == wxID_NO) {
	//		dlg->EndModal(1);
	//		closeWindow();
	//	}
	//	else {
	//		dlg->EndModal(1);
	//		return;
	//	}
	//}

	// Reads in data set to an array of doubles
	bool result = (dataType == "Audio") ? ProcessFile(audioFile, sfinfo) : ProcessFile(file, header);
	if (!result)
	{
		if (dlg->ShowModal() == wxID_NO) {
			dlg->EndModal(1);
			closeWindow();
		}
		else {
			dlg->EndModal(1);
			return;
		}
	}
	file.close();
	sf_close(audioFile);
	//fclose(f);
}

void ApplyChanges() {
	numClusters = wxAtoi(clustersText->GetValue());
	windowWidth = wxAtoi(winWidthText->GetValue());
	slidingWinWidth = wxAtoi(slidingWinText->GetValue());
	DCTSampleSize = wxAtoi(DCTSampleText->GetValue());
	while (!inferenceTimePeriod.empty()) {
		inferenceTimePeriod.erase(inferenceTimePeriod.begin());
	}
	istringstream timeText(timePeriodText->GetValue());
	while (timeText) {
		string s = "";
		if (!getline(timeText, s, ',' )) {
			break;
		}
		istringstream tempText(s);
		int period = 0;
		tempText >> period;
		inferenceTimePeriod.push_back(period);
	}
	while (!clusterAlphabet.empty()) {
		clusterAlphabet.erase(clusterAlphabet.begin());
	}
	
	if (!filename.empty() && dataType != "" && dataTypeSelect->GetSelection() != 0 && numClusters != 0 && windowWidth != 0 && slidingWinWidth != 0 && DCTSampleSize != 0 && !inferenceTimePeriod.empty()) {
		listBuilt = 0;
		blocksCreated = 0;
		maxOrig = 0;
		minOrig = 0;
		scaleY = 0.0f;
		offsetX = 0.0f;
		offsetY = 0.0f;
		zoomSpinner->SetValue(scaleY);
		if ((!timeseries.empty() && fileChanged) || normalise == false) {
			for (unsigned int a = 0; a < maxArrays; a++) {
				timeseries.erase(timeseries.begin());
				if (!signalLabels.empty())
					signalLabels.erase(signalLabels.begin());
			}
		}

		if (fileChanged || normalise == false) {
			Init();
		}
		applyBtn->Disable();
		statusText->SetLabel("Dataset Active");
		messagesText->SetLabel("You can now change the dataset or parameters dynamically. Once chosen, click Apply.");
		messagesText->Wrap(250);
		sbH->SetScrollbar(offsetX, windowWidth, dataSize*0.9855f, dataSize/windowWidth);
		sbV->SetScrollbar(sbV_Start, 50, sbV_Range, 100);
		
		zoomSpinner->Enable();
		columnSelect->Clear();

		for (unsigned int a = 0; a < maxArrays; a++) {
			if (!signalLabels.empty())
			{
				columnSelect->Append(signalLabels[a]);
			}
			else {
				stringstream integer;
				integer << a+1;
				columnSelect->Append(integer.str());
			}
		}
		columnSelect->Append("All");
		columnSelect->SetSelection(maxArrays);
		columnSelect->Enable();
		saveAlphaBtn->Enable();
		parentSizer->Layout();
		glSizer->Layout();
		vbox->Layout();
		if (!resetBtn->IsEnabled()) {
			resetBtn->Enable();
		}
	}
	else {
		wxMessageDialog *dlg = new wxMessageDialog(parentFrame,
			wxT("Please input all parameters"), wxT("Error parameters not set"),
			wxOK | wxICON_ERROR);
		if (dlg->ShowModal() == wxID_OK)
		{
			dlg->EndModal(1);
		}
	}

	fileChanged = false;
}

/* Key press processing */
void key(unsigned char c, int x, int y)
{
	if (c == 27) closeWindow();
};

void GLPane::keyPressed(wxKeyEvent& event) 
{
	switch(event.GetKeyCode()) {
		case WXK_LEFT:
			if (offsetX >= 0.0f) {
				offsetX = 0.0f;
			}
			else {
				offsetX += 100.0f;
				//b->x -= 100.04201805/(dataSize/(SCREEN_WIDTH-35));
			}
			sbH->SetThumbPosition(-offsetX);
			break;
		case WXK_RIGHT:
			if (offsetX <= (dataSize*-0.9855f)) {
				offsetX = (dataSize*-0.9855f);
			}
			else {
				offsetX -= 100.0f;
				//b->x += 100.04201805/(dataSize/(SCREEN_WIDTH-35));
			}
			sbH->SetThumbPosition(-offsetX);
			break;
		case WXK_UP:
			offsetY += 0.1f;
			sbV->SetThumbPosition(-offsetY+sbV_Start);
			break;
		case WXK_DOWN:
			offsetY -= 0.1f;
			sbV->SetThumbPosition(-offsetY+sbV_Start);
			break;
		case WXK_HOME:
			offsetX = 0.0f;
			sbH->SetThumbPosition(-offsetX);
			//b->x = 23;
			break;
		case WXK_END:
			offsetX = (dataSize*-0.9855f);
			sbH->SetThumbPosition(-offsetX);
			//b->x = SCREEN_WIDTH - 35;
			break;
	}
}

void GLPane::mouseWheelMoved(wxMouseEvent &event)
{
	int wheelRot = event.GetWheelRotation();
	if (wheelRot > 0) { // if positive scroll
		if (offsetX <= (dataSize*-0.9855f)) {
			offsetX = (dataSize*-0.9855f);
		}
		else {
			offsetX -= 100.0f;
			//b->x += 100.04201805/(dataSize/(SCREEN_WIDTH-35));
		}
		sbH->SetThumbPosition(-offsetX);
	}
	else if (wheelRot < 0) {
		if (offsetX >= 0.0f) {
			offsetX = 0.0f;
		}
		else {
			offsetX += 100.0f;
			//b->x -= 100.04201805/(dataSize/(SCREEN_WIDTH-35));
		}
		sbH->SetThumbPosition(-offsetX);
	}

}

//void processSpecialKeys(int key, int x, int y) {
//	switch(key) {
//		case GLUT_KEY_LEFT:
//			if (offsetX >= 0.0f) {
//				offsetX = 0.0f;
//			}
//			else {
//				offsetX += 100.0f;
//				//b->x -= 100.04201805/(dataSize/(SCREEN_WIDTH-35));
//			}
//			break;
//		case GLUT_KEY_RIGHT:
//			if (offsetX <= (dataSize*-0.9855f)) {
//				offsetX = (dataSize*-0.9855f);
//			}
//			else {
//				offsetX -= 100.0f;
//				//b->x += 100.04201805/(dataSize/(SCREEN_WIDTH-35));
//			}
//			break;
//		case GLUT_KEY_UP:
//			scaleY += 0.1f;
//			break;
//		case GLUT_KEY_DOWN:
//			scaleY -= 0.1f;
//			break;
//		case GLUT_KEY_HOME:
//			offsetX = 0.0f;
//			//b->x = 23;
//			break;
//		case GLUT_KEY_END:
//			offsetX = (dataSize*-0.9855f);
//			//b->x = SCREEN_WIDTH - 35;
//			break;
//	}
//}

void GLPane::resized(wxSizeEvent& evt)
{
	Update();
}

/* Window reshape */
//void reshape(int w, int h)
//{
//	SCREEN_WIDTH = w;
//	SCREEN_HEIGHT = h;
//	sb->set_w((0.8425034388)*SCREEN_WIDTH-15);
//	sb->set_h(SCREEN_HEIGHT-80);
//};


//void myGlutIdle( void )
//{
//  /* According to the GLUT specification, the current window is 
//     undefined during an idle callback.  So we need to explicitly change
//     it if necessary */
//  if (glutGetWindow() != window) 
//    glutSetWindow(window);  
//
//  glutPostRedisplay();
//}

void MyApp::control_cb(wxCommandEvent& event)
{
	int control = event.GetId();
	//glutPostRedisplay();
	if (control == SCROLLBARH_ID) {
		offsetX = -sbH->GetThumbPosition();
	}
	else if (control == SCROLLBARV_ID) {
		offsetY = -sbV->GetThumbPosition()+sbV_Start;
	}
	else if (control == CHECKBOX_LIST_ID) {
		for (int i = 0; i < displayCLB->GetCount(); i++) {
			if (displayCLB->IsChecked(i)) {
				switch (i) {
					case 0:
						origCheck = 1;
						break;
					case 1:
						dctCheck = 1;
						break;
					case 2:
						inverseCheck = 1;
						break;
					case 3:
						motifsCheck = 1;
						break;
					case 4:
						discreteCheck = 1;
						break;
				}
			}
			else {
				switch (i) {
					case 0:
						origCheck = 0;
						break;
					case 1:
						dctCheck = 0;
						break;
					case 2:
						inverseCheck = 0;
						break;
					case 3:
						motifsCheck = 0;
						break;
					case 4:
						discreteCheck = 0;
						break;
				}
			}
		}
		
	}
	else if (control == BROWSE_BUTTON_ID) {
		string oldFilename = "";
		//CString szFilter = "All Files(*.*)|*.*";
		wxFileDialog *openDlg = new wxFileDialog(parentFrame, _("Select Valid Dataset"), "", "",
				_("All Files(*.*)|*.*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);

		wxMessageDialog *dlg = new wxMessageDialog(parentFrame,
			wxT("Does the dataset file have a header line?"), wxT("Header line?"),
			wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION);
		//dlg(TRUE, _T("*.*"), NULL, OFN_FILEMUSTEXIST, szFilter);
		//dlg.m_ofn.lpstrTitle = _T("Select Valid Dataset");
		if(openDlg->ShowModal() == wxID_OK) {
			//CT2A temp(dlg.GetPathName());
			if (dlg->ShowModal() == wxID_YES)
			{
				header = true;
			}
			else {
				header = false;
			}
			dlg->EndModal(1);
			browseText->Enable();
			if (filename != "") {
				oldFilename = filename;
			}
			filename = openDlg->GetPath();
			if (filename == oldFilename) {
				fileChanged = false;
			}
			else {
				fileChanged = true;
			}

			browseText->SetLabel(filename);
			if (!clustersText->GetValue().empty() && !winWidthText->GetValue().empty() && 
					!slidingWinText->GetValue().empty() && !DCTSampleText->GetValue().empty() && 
					!timePeriodText->GetValue().empty()) {
				applyBtn->Enable();
			}
		}
				
		openDlg->EndModal(1);
	}
	else if (control == CLUSTER_TEXTBOX_ID) {
		if (!clustersText->GetValue().empty()) {
			applyBtn->Enable();
			//numClusters = wxAtoi(clustersText->GetValue());
		}
		else {
			applyBtn->Disable();
		}
	}
	else if (control == WINWIDTH_TEXTBOX_ID) {
		if (!winWidthText->GetValue().empty()) {
			applyBtn->Enable();
			//windowWidth = wxAtoi(winWidthText->GetValue());
		}
		else {
			applyBtn->Disable();
		}
	}
	else if (control == SLIDING_TEXTBOX_ID) {
		if (!slidingWinText->GetValue().empty()) {
			applyBtn->Enable();
			//slidingWinWidth = wxAtoi(slidingWinText->GetValue());
		}
		else {
			applyBtn->Disable();
		}
	}
	else if (control == DCTSAMPLE_TEXTBOX_ID) {
		if (!DCTSampleText->GetValue().empty()) {
			applyBtn->Enable();
			//DCTSampleSize = wxAtoi(DCTSampleText->GetValue());
		}
		else {
			applyBtn->Disable();
		}
	}
	else if (control == TIMEPERIOD_TEXTBOX_ID) {
		string timeText = timePeriodText->GetValue();
		if (!timeText.empty() && timeText != "," && timeText.substr(timeText.length()-1) != ",") {
			applyBtn->Enable();
		}
		else {
			applyBtn->Disable();
		}
	}
	else if (control == APPLY_BUTTON_ID) {
		string timeText = timePeriodText->GetValue();
		if ((!clustersText->GetValue().empty()) && (!winWidthText->GetValue().empty()) && (!slidingWinText->GetValue().empty()) && (!DCTSampleText->GetValue().empty()) && (!timeText.empty() || timeText[sizeof(timeText)-1] != ',') && (browseText->IsEnabled() == true)) {
			ApplyChanges();
		}
		else {
			wxMessageDialog *dlg = new wxMessageDialog(parentFrame,
					wxT("One of the parameters are empty/invalid\nor the dataset hasn't been chosen,\nplease fill them in."),
					wxT("Empty Parameters"), wxOK | wxCANCEL | wxICON_EXCLAMATION);
			if (dlg->ShowModal() == wxID_CANCEL) {
				closeWindow();
			}
			dlg->EndModal(1);
		}
	}
	else if (control == SAVE_BUTTON_ID) {
		//CString szFilter = "Text Files(*.txt)|*.txt|ASCII Files(*.ascii)|*.ascii|All Files(*.*)|*.*";
		//CFileDialog saveDlg(FALSE, _T("*.txt"), _T("Alphabet"), OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT, szFilter);
		//saveDlg.m_ofn.lpstrTitle = _T("Save alphabet");
		string fname = "Alphabet_" + columnSelect->GetStringSelection() + ".txt";
		wxFileDialog saveDlg(parentFrame, _("Save Alphabet"), "", fname,
				_("Text Files(*.txt)|*.txt|ASCII Files(*.ascii)|*.ascii|All Files(*.*)|*.*"),
				wxFD_SAVE | wxFD_OVERWRITE_PROMPT | wxFD_FILE_MUST_EXIST);
		if (saveDlg.ShowModal() == wxID_OK) {
			fstream alphaFile;
			string path = saveDlg.GetPath();
			alphaFile.open(path.c_str(), ios_base::out | ios_base::in);
			if(alphaFile.is_open()) {
				alphaFile.close();
				return;
			}
			else {
				alphaFile.clear();
				alphaFile.open(path.c_str(), ios_base::out);
				if (numArrays == maxArrays) {
					for (unsigned int a = 0; a < maxArrays; a++) {
						for (vector<string>::iterator alphaIter = clusterAlphabet[a].begin(); alphaIter != clusterAlphabet[a].end(); ++alphaIter) {
							alphaFile << (*alphaIter);
						}
					}
				}
				else {
					for (vector<string>::iterator alphaIter = clusterAlphabet[numArrays].begin(); alphaIter != clusterAlphabet[numArrays].end(); ++alphaIter) {
						alphaFile << (*alphaIter);
					}
				}
				alphaFile.close();
			}

		}

		saveDlg.EndModal(1);
	}
	else if (control == RULES_LIST_ID) {

	}
	else if (control == COLUMNSELECT_COMBOBOX_ID)
	{
		wxBusyCursor wait;
		numArrays = columnSelect->GetSelection();
		
		if (rulesList->GetItemCount() > 0) {
			rulesList->DeleteAllItems();
		}
		if (frequencyList->GetItemCount() > 0) {
			frequencyList->DeleteAllItems();
		}
		
		if (numArrays == maxArrays) {
			for (unsigned int a = 0; a < maxArrays; a++) {
				freqDisplayed = false;
				for (vector<int>::iterator timeIter = inferenceTimePeriod.begin(); timeIter != inferenceTimePeriod.end(); ++timeIter) {
					if (signalLabels.empty()) {
						ruleMining(clusterAlphabet[a],(*timeIter), a+1);
					}
					else {
						ruleMining(clusterAlphabet[a],(*timeIter),signalLabels[a]);
					}
					freqDisplayed = true;
				}
			}
		}
		else {
			freqDisplayed = false;
			for (vector<int>::iterator timeIter = inferenceTimePeriod.begin(); timeIter != inferenceTimePeriod.end(); ++timeIter) {
				if (signalLabels.empty()) {
					ruleMining(clusterAlphabet[numArrays],(*timeIter), numArrays);
				}
				else {
					ruleMining(clusterAlphabet[numArrays],(*timeIter),signalLabels[numArrays]);
				}
				freqDisplayed = true;
			}
		}
		
		rulesList->RefreshItems(0, rulesList->GetItemCount()-1);
		frequencyList->RefreshItems(0, frequencyList->GetItemCount()-1);
	}
	else if (control == DATATYPESELECT_COMBOBOX_ID)
	{
		if (dataType != dataTypeSelect->GetStringSelection()) {
			dataType = dataTypeSelect->GetStringSelection();
			dataType != "Please Select" ? applyBtn->Enable() : applyBtn->Disable();
		}
	}
	else if (control == ZOOM_SPINNER_ID) {
		scaleY = zoomSpinner->GetValue();
	}
	else if (control == QUIT_BUTTON_ID) {
		closeWindow();
	}
	else if (control == RESET_BUTTON_ID)
	{
		offsetX = offsetY = 0;

		sbV->SetThumbPosition(0);
		sbH->SetThumbPosition(0);
		scaleY = 0;
		zoomSpinner->SetValue(scaleY);
		sbH->SetScrollbar(offsetX, windowWidth, dataSize*0.9855f, dataSize/windowWidth);
		sbV->SetScrollbar(sbV_Start, 50, sbV_Range, 100);
	}
	else if (control == NORMALISE_CB_ID)
	{
		if (normaliseCB->IsChecked()) {
			normalise = true;
		}
		else {
			normalise = false;
		}
		applyBtn->Enable();
	}
}
