#ifndef _HELPER_H
#define _HELPER_H

#include <string>
#include <numeric>
#include <vector>
#include <algorithm>
#include <functional>
#include <assert.h>

using namespace std;

typedef struct Point
{
	double x;
	float y;
	int clusterIndex;
} Point;

typedef struct Clusters
{
	float x;
	float y;
	int totalMembers;
} Clusters;

class Keys {
public:
	Keys(string k1, string k2, int t) : key1(k1), key2(k2), timeUnits(t) { }
	bool operator<(const Keys &right) const {
		return (key1 < right.key1 && key2 < right.key2);
	}
	bool operator==(const Keys &right) const {
		return (key1 == right.key1 && key2 == right.key2 && timeUnits == right.timeUnits);
	}

	string key1, key2;
	int timeUnits;
};

enum GLUI_ID {
	SCROLLBARH_ID,
	SCROLLBARV_ID,
	NORMALISE_CB_ID,
	BROWSE_BUTTON_ID,
	RESET_BUTTON_ID,
	CLUSTER_TEXTBOX_ID, 
	WINWIDTH_TEXTBOX_ID, 
	SLIDING_TEXTBOX_ID, 
	DCTSAMPLE_TEXTBOX_ID,
	TIMEPERIOD_TEXTBOX_ID,
	APPLY_BUTTON_ID,
	RULES_LIST_ID,
	FREQUENCY_LIST_ID,
	CHECKBOX_LIST_ID,
	COLUMNSELECT_COMBOBOX_ID,
	DATATYPESELECT_COMBOBOX_ID,
	ZOOM_SPINNER_ID,
	SAVE_BUTTON_ID,
	QUIT_BUTTON_ID
};

int numdigits(int n)
{
	int count = 0;
	for (int i = 1; n != 0; ++i) {
		n /= 10;
		count = i;
	}
	return count;
};

bool is_digits(const std::string &str)
{
    return str.find_first_not_of("0123456789") == std::string::npos;
}

template<typename K, typename V>
pair<const V, K> flip_pair(const pair<const K, V>& p)
{
     return pair<const V, K>(p.second, p.first);
}

template<typename K, typename V>
multimap<V, K> invert_map(const map<K, V>& in)
{
     multimap<V, K> out;
     transform(in.begin(), in.end(), inserter(out, out.begin()), flip_pair<K, V>);
     return out;
}

template<typename T>
vector<vector<T> > operator-(const vector<vector<T> >& a, const vector<vector<T> >& b)
{
	assert(a.size() == b.size());

	vector<vector<T> > result;
	result.resize(a.size());
	typedef typename vector<vector<T> >::const_iterator iter;
	int row = 0;
	for (pair<iter, iter> i(a.begin(),b.begin()); i.first != a.end() && i.second != b.end(); ++i.first, ++i.second) {
		transform((*i.first).begin(), (*i.first).end(), (*i.second).begin(), back_inserter(result[row]), minus<T>());
		row++;
	}

	return result;
}

template<typename T>
vector<vector<T> > operator/=(vector<vector<T> >& a, const vector<vector<T> >& b)
{
	assert(a.size() == b.size());

	vector<vector<T> > result;
	result.resize(a.size());
	typedef typename vector<vector<T> >::const_iterator iter;
	int row = 0;
	for (pair<iter, iter> i(a.begin(),b.begin()); i.first != a.end() && i.second != b.end(); ++i.first, ++i.second) {
		transform((*i.first).begin(), (*i.first).end(), (*i.second).begin(), back_inserter(result[row]), divides<T>());
		row++;
	}

	a = result;
	return a;
}

template<typename T>
vector<vector<T> > transpose(vector<vector<T> > out)
{
	vector<vector<double> > outtrans(out[0].size(), vector<double>(out.size()));
	for (size_t i = 0; i < out.size(); ++i)
	    for (size_t j = 0; j < out[0].size(); ++j)
	        outtrans[j][i] = out[i][j];

	return outtrans;
}

template<typename T>
vector<vector<T> > tile(vector<T> vec, int reps)
{
	vector<vector<T> > result;
	result.resize(reps);
	for (int i = 0; i < reps; i++) {
		result[i].resize(vec.size());
		copy(vec.begin(), vec.end(), result[i].begin());
	}
	return result;
}

vector<double> mean(vector<vector<double> >& vec)
{
	vector<double> result;
	for (vector<vector<double> >::iterator iter = vec.begin(); iter != vec.end(); ++iter) {
		result.push_back(accumulate((*iter).begin(), (*iter).end(), 0)/(*iter).size());
	}
	return result;
}

vector<double> stdDev(vector<vector<double> >& vec)
{
	vector<double> result;
	for (vector<vector<double> >::iterator iter = vec.begin(); iter != vec.end(); ++iter) {
		double mean;// = accumulate((*iter).begin(), (*iter).end(), 0)/(*iter).size();
		vector<double> diff((*iter).size());
		transform((*iter).begin(), (*iter).end(), diff.begin(), bind2nd(minus<double>(), mean));
		double sq_sum = inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
		double stdev = sqrt(sq_sum / (*iter).size());
		result.push_back(stdev);
	}
	return result;
}

#endif
