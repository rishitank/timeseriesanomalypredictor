# Builds all the projects in the solution...
.PHONY: all_projects
all_projects: TimeSeriesAnomalyPredictor 

# Builds project 'TimeSeriesAnomalyPredictor'...
.PHONY: TimeSeriesAnomalyPredictor
TimeSeriesAnomalyPredictor: 
	make --directory="." --file=TimeSeriesAnomalyPredictor.makefile

# Cleans all projects...
.PHONY: clean
clean:
	make --directory="." --file=TimeSeriesAnomalyPredictor.makefile clean

