# Compiler flags...
CPP_COMPILER = g++
C_COMPILER = gcc

# Include paths...
Debug_Include_Path=`wx-config --cxxflags`
Release_Include_Path=`wx-config --cxxflags`

# Library paths...
Debug_Library_Path=`wx-config --libs --gl-libs` -Wl,-lsndfile
Release_Library_Path=`wx-config --libs --gl-libs` -Wl,-lsndfile

# Additional libraries...
Debug_Libraries=-Wl,-lsndfile
#-Wl,-arch,x86_64,-arch_multiple
#--start-group -l* -lkernel32 -luser32 -lgdi32 -lwinspool -lcomdlg32 -ladvapi32 -lshell32 -lole32 -loleaut32 -luuid -lodbc32 -lodbccp32  -Wl,--end-group
Release_Libraries=-Wl,-lsndfile

output=TimeSeriesAnomalyPredictor
package=.
# Preprocessor definitions...
ifeq ($(OS),Windows_NT)
    SYS=$(OS)
    CCFLAGS += -D WIN32
    package=$(output).exe
    ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
        CCFLAGS += -D AMD64
    endif
    ifeq ($(PROCESSOR_ARCHITECTURE),x86)
        CCFLAGS += -D IA32
    endif
else
    UNAME_S := $(shell uname -s)
    SYS=$(UNAME_S)
    ifeq ($(UNAME_S),Linux)
        CCFLAGS += -D LINUX
	Debug_Libraries+=-Wl,-lGL,-lGLU,-lglut
	Release_Libraries+=-Wl,-lGL,-lGLU,-lglut
    endif
    ifeq ($(UNAME_S),Darwin)
	package=$(output).app/Contents/MacOS
        CCFLAGS += -D OSX
	Debug_Library_Path += -framework OpenGL -framework GLUT
	Release_Library_Path += -framework OpenGL -framework GLUT
	UNAME_P := $(shell uname -m)
    else
	UNAME_P := $(shell uname -p)
    endif
    ifeq ($(UNAME_P),x86_64)
        CCFLAGS += -D AMD64
    endif
    ifneq ($(filter %86,$(UNAME_P)),)
        CCFLAGS += -D IA32
    endif
    ifneq ($(filter arm%,$(UNAME_P)),)
        CCFLAGS += -D ARM
    endif
endif
Debug_Preprocessor_Definitions=$(CCFLAGS) -D GCC_BUILD -D _DEBUG -D _CONSOLE -D _ITERATOR_DEBUG_LEVEL=0 -D _WCHAR_H_CPLUSPLUS_98_CONFORMANCE_
Release_Preprocessor_Definitions=$(CCFLAGS) -D GCC_BUILD -D NDEBUG -D _CONSOLE -D _WCHAR_H_CPLUSPLUS_98_CONFORMANCE_

# Implictly linked object files...
Debug_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=

# Compiler flags...
Debug_Compiler_Flags=-O0 -g -std=c++11
Release_Compiler_Flags=-O2

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug Release 

# Builds the Debug configuration...
.PHONY: Debug
Debug: create_folders gccDebug/src/Main.o gccDebug/src/StdAfx.o 
	$(CPP_COMPILER) gccDebug/src/$(SYS)/Main.o gccDebug/src/$(SYS)/StdAfx.o $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,./ -o gccBin/Debug/$(SYS)/$(package)/$(output)

# Compiles file src/Main.cpp for the Debug configuration...
-include gccDebug/src/Main.d
gccDebug/src/Main.o: src/Main.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/Main.cpp $(Debug_Include_Path) -o gccDebug/src/$(SYS)/Main.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/Main.cpp $(Debug_Include_Path) > gccDebug/src/$(SYS)/Main.d

# Compiles file src/StdAfx.cpp for the Debug configuration...
-include gccDebug/src/StdAfx.d
gccDebug/src/StdAfx.o: src/StdAfx.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/StdAfx.cpp $(Debug_Include_Path) -o gccDebug/src/$(SYS)/StdAfx.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/StdAfx.cpp $(Debug_Include_Path) > gccDebug/src/$(SYS)/StdAfx.d

# Builds the Release configuration...
.PHONY: Release
Release: create_folders gccRelease/src/Main.o gccRelease/src/StdAfx.o 
	g++ gccRelease/src/$(SYS)/Main.o gccRelease/src/$(SYS)/StdAfx.o  $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,./ -o gccBin/Release/$(SYS)/$(package)/$(output)

# Compiles file src/Main.cpp for the Release configuration...
-include gccRelease/src/Main.d
gccRelease/src/Main.o: src/Main.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/Main.cpp $(Release_Include_Path) -o gccRelease/src/$(SYS)/Main.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/Main.cpp $(Release_Include_Path) > gccRelease/src/$(SYS)/Main.d

# Compiles file src/StdAfx.cpp for the Release configuration...
-include gccRelease/src/StdAfx.d
gccRelease/src/StdAfx.o: src/StdAfx.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/StdAfx.cpp $(Release_Include_Path) -o gccRelease/src/$(SYS)/StdAfx.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/StdAfx.cpp $(Release_Include_Path) > gccRelease/src/$(SYS)/StdAfx.d

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p gccDebug/src/$(SYS)
	mkdir -p gccBin/Debug/$(SYS)/$(package)
	mkdir -p gccRelease/src/$(SYS)
	mkdir -p gccBin/Release/$(SYS)/$(package)

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -rf gccDebug/src/$(SYS)/*
	rm -rf gccBin/$(SYS)/*
	rm -rf gccRelease/src/$(SYS)/*

